﻿using UnityEngine;
using System.Collections;

public class GrabObject : MonoBehaviour {

    private SteamVR_TrackedObject trackedObject;
    private SteamVR_Controller.Device device;

    public float throwSpeed = 2;

	// Use this for initialization
	void Start () {
        trackedObject = GetComponent<SteamVR_TrackedObject>();
	}
	
	// Update is called once per frame
	void Update () {
        device = SteamVR_Controller.Input((int)trackedObject.index);
	}
    
    void OnTriggerStay(Collider col)
    {
        if (col.CompareTag("Ball"))
        {
            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                col.GetComponent<Rigidbody>().isKinematic = true;
                col.transform.SetParent(transform);
            }

            if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
            {
                col.GetComponent<Rigidbody>().isKinematic = false;
                col.transform.SetParent(null);

                TossObject(col.attachedRigidbody);
            }
        }
    }

    private void TossObject(Rigidbody rb)
    {
        rb.velocity = device.velocity * throwSpeed;
        rb.angularVelocity = device.angularVelocity;
    }
}
