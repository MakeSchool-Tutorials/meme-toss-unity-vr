﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static int score;
    public static int level;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnLevelWasLoaded()
    {

        BallSpawner.totalBallsDestroyed = 0;
        BallSpawner.totalBallsSpawned = 0;
        BallSpawner.ballsInPlay = 0;

        ScreenFader.fadeDown = false;
        ScreenFader.fadeUp = true;
        BallSpawner.isLoadingScene = false;
        
    }
}
