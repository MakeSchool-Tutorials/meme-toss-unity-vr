﻿using UnityEngine;
using System.Collections;

public class ScreenFader : MonoBehaviour {

    private Renderer effectRenderer;
    private Color effectColor;
    public float lerpPeriod = 2;
    private float lerpTimer;

    public static bool fadeUp;
    public static bool fadeDown;

	// Use this for initialization
	void Start () {
        effectRenderer = GetComponent<MeshRenderer>();

        fadeUp = true;
	}
	
	// Update is called once per frame
	void Update () {

        if (fadeDown)
        {
            lerpTimer += Time.deltaTime;
        }
        if (fadeUp)
        {
            lerpTimer -= Time.deltaTime;
            fadeUp = false;
        }

        lerpTimer = Mathf.Clamp(lerpTimer, 0, lerpPeriod);

        effectColor = Color.Lerp(Color.clear, Color.black, lerpTimer);
        effectRenderer.material.color = effectColor;
	}
}
