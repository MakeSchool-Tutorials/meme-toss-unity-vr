﻿using UnityEngine;
using System.Collections;

public class MoveGoal : MonoBehaviour {

    private float timer;
    public float period;
    public float range;
    public float dPeriod;

    private float xStart;

	// Use this for initialization
	void Start () {
        xStart = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {

        timer -= Time.deltaTime;
        if (timer < 0)
        {
            timer = period;
        }

        float percentage = period == 0 ? 0 : timer / period;
        float dx = range * Mathf.Sin(Mathf.PI * 2 * percentage);

        transform.position = new Vector3(xStart + dx,
            transform.position.y,
            transform.position.z);
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Ball"))
        {
            period -= dPeriod;
        }
    }
}
