﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnCollisionEnter(Collision col)
    {
        BallVariables ball = col.gameObject.GetComponent<BallVariables>();
        
        if (ball && !ball.hasHitWall)
        {
            ball.hasHitWall = true;
            GameManager.score++;
        }
    }
}
