﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BallSpawner : MonoBehaviour {

    public GameObject ball;
    public static int ballsInPlay;

    public float timeSinceLastBallSpawned;
    public float timeBetweenBallSpawns = 0.6f;

    public static int ballsInLevel = 8;
    public static int totalBallsDestroyed;
    public static int totalBallsSpawned;

    public static bool isLoadingScene;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
        if (timeSinceLastBallSpawned < Time.time)
        {
            if (totalBallsSpawned < ballsInLevel && ballsInPlay < 2)
            {
                SpawnBall();

                timeSinceLastBallSpawned = Time.time + timeBetweenBallSpawns;
            }
        }

        if (totalBallsDestroyed >= ballsInLevel && !isLoadingScene)
        {
            Invoke("LoadLevel", 1);
            isLoadingScene = true;
            ScreenFader.fadeDown = true;
        }
        
	}

    void LoadLevel()
    {
        totalBallsDestroyed = 0;
        totalBallsSpawned = 0;
        ballsInPlay = 0;
        GameManager.score = 0;
        GameManager.level++;
        SceneManager.LoadScene("Scene" + GameManager.level);
    }

    void SpawnBall()
    {
        ballsInPlay++;
        totalBallsSpawned++;
        Instantiate(ball, transform.position, Quaternion.identity);
    }
}
