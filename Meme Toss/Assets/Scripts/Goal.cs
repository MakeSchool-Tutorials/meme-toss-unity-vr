﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {

    public ParticleSystem ps;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Ball"))
        {
            GameManager.score += 5;
            Destroy(col.gameObject);
            BallSpawner.ballsInPlay--;
            BallSpawner.totalBallsDestroyed++;

            ps.Play();
            //Invoke("StopParticles", 1);
        }
    }

    void StopParticles()
    {
        ps.Stop();
    }
}
